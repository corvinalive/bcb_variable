//---------------------------------------------------------------------------

#ifndef variableH
#define variableH
#include <vcl.h>
#include <Inifiles.hpp>
//---------------------------------------------------------------------------
double Man10(double x, int& x10);
void  FormatPrecDigit(double value, int WantedDigits,AnsiString& result);
//---------------------------------------------------------------------------
class TMICommonData
{
};
//---------------------------------------------------------------------------
class TCalcOptions
{
public:
   enum TRoundOptions {None=0, AfterDot=1, SignDigits=2 }
      CalcRoundType, ShowRoundType;
   int CalcRoundNumber;
   int ShowRoundNumber;
};
//---------------------------------------------------------------------------
class TVar
{
public:
   enum TState {UserInput=0,UsedNearest=1,ErrorValue=2, Calculated=3,ErrorArg=4,
               ErrorResult=5,NotUsed=6} State;

   union TValue
      {
      int i;
      double d;
      char* c;
      bool b;
      } Value;
   union TUserData
      {
      int i[2];
      double d;
      char c[8];
      bool b;
      } UserData;
   __fastcall TVar(){Value.d=0;State=UsedNearest;};
   static const VarColorCount=14;
   static TColor Colors[VarColorCount];
};
//---------------------------------------------------------------------------
class TVarOptions
{
public:
   enum TVarType {Integer=0,Float=1,Bool=2, List=3, Text=4} Type;
   enum TVarKind {InputData=0,CalculatedData=1} Kind;
   AnsiString Name;
   AnsiString Description;
   AnsiString Unit;

   TCalcOptions CalcOptions;
   int Width;
   int Bitmap;
   int UnitBitmap;
   TStrings* Strings;
   __fastcall TVarOptions()
      {
      Strings=NULL;
      }
   __fastcall ~TVarOptions()
      {
      if(Strings!=NULL)
         delete Strings;
      }
   void __fastcall LoadFromIni(TIniFile* ini,AnsiString s);
   void __fastcall FormatStr(TVar* Var,AnsiString& Text);
   void __fastcall Save(IStream* Stream);
   void __fastcall Load(IStream* Stream);
   void __fastcall ProcessRound(TVar* Var);
   static AnsiString __fastcall FormatSD(double value, int WantedDigits/*,AnsiString& result*/);
   static AnsiString __fastcall FormatDAD(double value, int WantedDigits/*,AnsiString& result*/);
};
//---------------------------------------------------------------------------
//--- virtual base classes --------------------------------------------------
//---------------------------------------------------------------------------
class TVarContainer
{
private:
   virtual TVar* __fastcall GetVar(int __index)=0;
   virtual int __fastcall GetVarCount()=0;
public:
   __property TVar* Var[int index]  = { read=GetVar };
   virtual void __fastcall Load(IStream* Stream)=0;
   virtual void __fastcall Save(IStream* Stream)=0;
   __property int VarCount  = { read=GetVarCount }; //Size of array Var
};
//---------------------------------------------------------------------------
class TGridPoint:public TVarContainer
{
private:
   virtual TVarContainer*  __fastcall GetMeasures(int index)=0;
   virtual int __fastcall GetCount()=0;
   virtual int __fastcall GetMeasureVarCount()=0;
public:
   __property TVarContainer* Measures[int index]  = { read=GetMeasures };
   __property int Count  = { read=GetCount }; //Count of Measures
   virtual int __fastcall Add(TVarContainer*)=0;
   virtual int __fastcall AddNew()=0;
   virtual void __fastcall Delete(int Index)=0;
   virtual void __fastcall ClearMeasures()=0;
   //Size of Measure's Var[] array
   __property int MeasureVarCount  = { read=GetMeasureVarCount };
};
//---------------------------------------------------------------------------
class TIntList
{
private:
   TList* FList;
   union
      {
      int i;
      void* p;
      } pi;
   int  __fastcall GetValue(int index){pi.p=FList->Items[index];return pi.i;};
	void  __fastcall SetValue(int index,int Value){pi.i=Value; FList->Items[index]=pi.p;};
	int  __fastcall GetCount(){ return FList->Count;};
public:
	__fastcall TIntList() {FList = new TList();};
	__fastcall ~TIntList() {delete FList;};
	void __fastcall Clear(){FList->Clear();};
	void __fastcall Delete(int Index) {FList->Delete(Index);};
	int __fastcall Add(int Value){ pi.i=Value; return FList->Add(pi.p);};
	void __fastcall Insert(int Index, int Value)
		{pi.i=Value; FList->Insert(Index,pi.p);};
	__property int Values[int index]  = { read=GetValue,write = SetValue };
   __property int Count={read=GetCount};
   __property TList* List={read=FList};
	void __fastcall Save(IStream* Stream);
   void __fastcall Load(IStream* Stream);
	void __fastcall Assign(TIntList* Source);
	void __fastcall Exclude(TIntList* Excluded);
   void __fastcall Move(int CurIndex, int NewIndex);
   int __fastcall IndexOf(int Item);
   int __fastcall Remove(int Item);
};
//---------------------------------------------------------------------------
enum TRowType {PointRow=0,MeasureRow=1};
//---------------------------------------------------------------------------
struct TMICellData
{
   TVarOptions* vo;
   TVar* v;
   TRowType Type;
   int VarIndex;
   int PointIndex;
   int MeasureIndex;
};
//---------------------------------------------------------------------------
typedef Graphics::TBitmap* __fastcall (__closure *TGetBitmapEvent)(int index);
//---------------------------------------------------------------------------
class TGridData
{
protected:
   TNotifyEvent FOnChangeItemsStructure;
   virtual TGridPoint* __fastcall GetGridPoints(int index)=0;
   virtual TGridPoint* __fastcall GetUsedPoints(int index)=0;
   virtual int __fastcall GetCount()=0;
   virtual TVarOptions* __fastcall GetPointVarOptions(int index)=0;
   virtual TVarOptions* __fastcall GetMeasureVarOptions(int index)=0;
   virtual TVarOptions* __fastcall GetMIVarOptions(int index)=0;
   TNotifyEvent FOnChange;
public:
   TIntList MeasureColumns;
   TIntList PointColumns;
   __property TGridPoint* GridPoints[int index]  = { read=GetGridPoints};
   __property TGridPoint* UsedPoint[int index]  = { read=GetUsedPoints};
   __property TVarOptions* PointVarOptions[int index]  = { read=GetPointVarOptions};
   __property TVarOptions* MeasureVarOptions[int index]  = { read=GetMeasureVarOptions};
   __property TVarOptions* MIVarOptions[int index]  = { read=GetMIVarOptions};
   __property int Count  = { read=GetCount };
   virtual int __fastcall Add(TGridPoint*)=0;
   virtual int __fastcall AddNew()=0;
   virtual int __fastcall AddNewMeasure(int PointIndex)=0;
   virtual void __fastcall Delete(int Index)=0;
   virtual void __fastcall SetValue(TVar::TValue Value, TMICellData* Data)=0;
   virtual bool __fastcall CanEdit(TMICellData* Data);
   virtual void __fastcall ModifyPopupMenu(TPopupMenu* Menu,TMICellData* Data)
      {Application->MessageBox("ModifyPopupMenu","Ok",0);};
   virtual void __fastcall Load(IStream* Stream)=0;
   virtual void __fastcall Save(IStream* Stream)=0;
   virtual void __fastcall ClearPoints()=0;
   virtual void __fastcall Calculate()=0;
   __fastcall TGridData(TMICommonData* CommonData);
   __property TGetBitmapEvent OnGetBitmap  = { read=FOnGetBitmap, write=FOnGetBitmap };
   __property TMICommonData* MICommonData  = { read=FMICommonData };
   __property int GridVarCount  = { read=GetGridVarCount };
   __property int PointVarCount  = { read=GetPointVarCount };
   __property int MeasureVarCount  = { read=GetMeasureVarCount };
   __property TNotifyEvent OnChange  = { read=FOnChange, write=FOnChange };
   __property TNotifyEvent OnChangeItemsStructure  = { read=FOnChangeItemsStructure, write=FOnChangeItemsStructure };
private:
   TGetBitmapEvent FOnGetBitmap;
   TMICommonData* FMICommonData;
   virtual int __fastcall GetGridVarCount()=0;
   virtual int __fastcall GetPointVarCount()=0;
   virtual int __fastcall GetMeasureVarCount()=0;
};
//---------------------------------------------------------------------------
#endif

